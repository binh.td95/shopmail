﻿using System;
using Microsoft.EntityFrameworkCore;
using ShopMail.Models;
namespace ShopMail.Data
{
    public class ShopDbContext : DbContext
    {
        public ShopDbContext(DbContextOptions options) : base(options)
        {

        }
        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<ProductImage> Product_images { get; set; }
        public DbSet<ProductCategory> Product_categories { get; set; }
        public DbSet<User> Users { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>().ToTable("products");
            modelBuilder.Entity<Category>().ToTable("categories");
            modelBuilder.Entity<ProductImage>().ToTable("product_images");
            modelBuilder.Entity<ProductCategory>().ToTable("product_categories");
            modelBuilder.Entity<User>().ToTable("users");
        }
    }
}

