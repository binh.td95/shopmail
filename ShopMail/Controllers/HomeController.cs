﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using ShopMail.Interface;
using ShopMail.Models;

namespace ShopMail.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;

    private readonly ICategory _categoryRepo;
    private readonly IProduct _productRepo;

    public HomeController(ILogger<HomeController> logger, ICategory category, IProduct product)
    {
        _logger = logger;
        _categoryRepo = category;
        _productRepo = product;
    }

    public async Task<IActionResult> Index()
    {
        var categories = await _categoryRepo.GetAllCategoryAsync();
        var products = await _productRepo.GetAllProductAsync();

        ViewData["Categories"] = categories;
        ViewData["Products"] = products;

        return View();
    }

    public IActionResult Login()
    {
        return Redirect("/Authentication/Login");
    }
    public IActionResult Register()
    {
        return Redirect("/Authentication/Register");
    }
    public IActionResult About()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}

