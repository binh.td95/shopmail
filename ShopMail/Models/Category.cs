﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ShopMail.Models
{
    public class Category
    {
        public int ID { get; set; }
        public string? Name { get; set; }
    }
}

