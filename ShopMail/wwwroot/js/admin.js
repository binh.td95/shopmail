﻿const $ = document.querySelector.bind(document);
const $$ = document.querySelectorAll.bind(document);

const tabs = $$(".tab-item");
const contents = $$(".tab-pane");

const tabActive = $(".tab-item.active");

tabs.forEach((tab, index) => {
    const content = contents[index];

    tab.onclick = function () {
        $(".tab-item.active").classList.remove("active");
        $(".tab-pane.active").classList.remove("active");

        this.classList.add("active");
        content.classList.add("active");
    };
});

var modal = document.getElementById("myModal");
var btSave = document.getElementById("save");
var btCancel = document.getElementById("cancel");
var btAdd = document.getElementById("add");

btAdd.onclick = function () {
    modal.style.display = "flex";
};

btCancel.onclick = function () {
    modal.style.display = "none";
};
