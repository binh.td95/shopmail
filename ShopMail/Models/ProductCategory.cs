using System;
namespace ShopMail.Models
{
    public class ProductCategory
    {
        public int ID { get; set; }
        public decimal ProductId { get; set; }
        public decimal CategoryId { get; set; }
    }
}