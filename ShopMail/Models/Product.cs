﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ShopMail.Models
{
    public class Product
    {
        public int ID { get; set; }
        public string? Name { get; set; }
        public decimal Price { get; set; }
        public decimal SalePrice { get; set; }
        public string? Description { get; set; }
        public int Status { get; set; }
    }
}

