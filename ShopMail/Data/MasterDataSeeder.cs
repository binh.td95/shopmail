using ShopMail.Models;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System.Security.Cryptography;
namespace ShopMail.Data
{
    public static class MasterDataSeeder
    {
        public static void Initialize(ShopDbContext context)
        {
            if (context.Users.Any())
            {
                return;   // DB has been seeded
            }
            byte[] salt = RandomNumberGenerator.GetBytes(128 / 8); // divide by 8 to convert bits to bytes
            Console.WriteLine($"Salt: {Convert.ToBase64String(salt)}");
            string Password = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: "Amela@123",
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA256,
                iterationCount: 100000,
                numBytesRequested: 256 / 8));
            // string Password= "Amela@123";
            var users = new User[]
            {
                new User{FirstName="Duy Binh", LastName="Truong",Email="binh.truongduy@amela.vn",Avatar="",Phone="1231233213123",Address="amela",Password=Password,Role=2},
                new User{FirstName="Duy Binh", LastName="Truong",Email="binh.truongduy+1@amela.vn",Avatar="",Phone="1231233213123",Address="amela",Password=Password,Role=2},
                new User{FirstName="Duy Binh", LastName="Truong",Email="binh.truongduy+2@amela.vn",Avatar="",Phone="1231233213123",Address="amela",Password=Password,Role=2},
                new User{FirstName="Duy Binh", LastName="Truong",Email="binh.truongduy+3@amela.vn",Avatar="",Phone="1231233213123",Address="amela",Password=Password,Role=2},
                new User{FirstName="Duy Binh", LastName="Truong",Email="binh.truongduy+4@amela.vn",Avatar="",Phone="1231233213123",Address="amela",Password=Password,Role=2},
                new User{FirstName="Duy Binh", LastName="Truong",Email="binh.truongduy+5@amela.vn",Avatar="",Phone="1231233213123",Address="amela",Password=Password,Role=2},
            };
            context.Users.AddRange(users);
            context.SaveChanges();

            var categories = new Category[]
            {
                new Category{Name="Categoty 1"},
                new Category{Name="Categoty 2"},
                new Category{Name="Categoty 3"},
                new Category{Name="Categoty 4"},
            };
            context.Categories.AddRange(categories);
            context.SaveChanges();

            var products = new Product[]
            {
                new Product{Name="product 1",Description="mo ta",Price=19999,SalePrice=10000,Status=1},
                new Product{Name="product 2",Description="mo ta",Price=19999,SalePrice=10000,Status=1},
                new Product{Name="product 3",Description="mo ta",Price=19999,SalePrice=10000,Status=1},
                new Product{Name="product 4",Description="mo ta",Price=19999,SalePrice=10000,Status=1},
                new Product{Name="product 5",Description="mo ta",Price=19999,SalePrice=10000,Status=1},
                new Product{Name="product 6",Description="mo ta",Price=19999,SalePrice=10000,Status=1},
                new Product{Name="product 7",Description="mo ta",Price=19999,SalePrice=10000,Status=1},
                new Product{Name="product 8",Description="mo ta",Price=19999,SalePrice=10000,Status=1},
                new Product{Name="product 9",Description="mo ta",Price=19999,SalePrice=10000,Status=1},
                new Product{Name="product 10",Description="mo ta",Price=19999,SalePrice=10000,Status=1}
            };

            context.Products.AddRange(products);
            context.SaveChanges();

            var product_images = new ProductImage[]
            {
                new ProductImage{ProductId=1,Image="dldx7jgktmo6o.cloudfront.net/product/8242023-03-03 18:35:12/image/4G0qS54HrI/详情图片_01.jpeg",Sort=1},
                new ProductImage{ProductId=2,Image="dldx7jgktmo6o.cloudfront.net/product/8242023-03-03 18:35:12/image/4G0qS54HrI/详情图片_01.jpeg",Sort=1},
                new ProductImage{ProductId=3,Image="dldx7jgktmo6o.cloudfront.net/product/8242023-03-03 18:35:12/image/4G0qS54HrI/详情图片_01.jpeg",Sort=1},
                new ProductImage{ProductId=4,Image="dldx7jgktmo6o.cloudfront.net/product/8242023-03-03 18:35:12/image/4G0qS54HrI/详情图片_01.jpeg",Sort=1},
                new ProductImage{ProductId=5,Image="dldx7jgktmo6o.cloudfront.net/product/8242023-03-03 18:35:12/image/4G0qS54HrI/详情图片_01.jpeg",Sort=1},
                new ProductImage{ProductId=6,Image="dldx7jgktmo6o.cloudfront.net/product/8242023-03-03 18:35:12/image/4G0qS54HrI/详情图片_01.jpeg",Sort=1},
                new ProductImage{ProductId=7,Image="dldx7jgktmo6o.cloudfront.net/product/8242023-03-03 18:35:12/image/4G0qS54HrI/详情图片_01.jpeg",Sort=1},
                new ProductImage{ProductId=8,Image="dldx7jgktmo6o.cloudfront.net/product/8242023-03-03 18:35:12/image/4G0qS54HrI/详情图片_01.jpeg",Sort=1},
                new ProductImage{ProductId=9,Image="dldx7jgktmo6o.cloudfront.net/product/8242023-03-03 18:35:12/image/4G0qS54HrI/详情图片_01.jpeg",Sort=1},
                new ProductImage{ProductId=10,Image="dldx7jgktmo6o.cloudfront.net/product/8242023-03-03 18:35:12/image/4G0qS54HrI/详情图片_01.jpeg",Sort=1}
            };
            context.Product_images.AddRange(product_images);
            context.SaveChanges();

            var product_categories = new ProductCategory[]
            {
                new ProductCategory{ProductId=1,CategoryId=1},
                new ProductCategory{ProductId=2,CategoryId=1},
                new ProductCategory{ProductId=3,CategoryId=2},
                new ProductCategory{ProductId=4,CategoryId=2},
                new ProductCategory{ProductId=5,CategoryId=3},
                new ProductCategory{ProductId=6,CategoryId=4},
            };
            context.Product_categories.AddRange(product_categories);
            context.SaveChanges();
        }
    }
}