﻿using System;
using ShopMail.Models;

namespace ShopMail.Interface
{
	public interface ICategory 
	{
        public Task<List<Category>> GetAllCategoryAsync();

        public Task<Category> GetCategoryAsync(int id);

        public Task<Category> AddCategoryAsync(Category category);

        public Task UpdateCategoryAsync(int id, Category category);

        public Task DeleteCategoryAsync(int id);
    }
}

