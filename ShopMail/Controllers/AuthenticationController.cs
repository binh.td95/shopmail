﻿using Azure;
using System.Linq;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using ShopMail.Data;
using ShopMail.Models;
using System.Security.Cryptography;

namespace ShopMail.Controllers
{
    public class AuthenticationController : Controller
    {
        private readonly ShopDbContext _db;

        public AuthenticationController (ShopDbContext db)
        {
            _db = db;
        }

        //Get: register

        public IActionResult Register()
        {
            return View();
        }

        //Post register
        [HttpPost]
        public IActionResult Register([FromBody]User _users)
        {

            if (!ModelState.IsValid)
            {
                return View(_users);
            }
            else
            {
                var isEmailExist = _db.Users.Count(x => x.Email == _users.Email) > 0;

                if (!isEmailExist)               
                {
                    string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                        password: _users.Password,
                        salt: RandomNumberGenerator.GetBytes(128 / 8), //random khoa cong khai
                        prf: KeyDerivationPrf.HMACSHA256, //lay khoa con 256 bit
                        iterationCount: 10000, //lap 10000 lan
                        numBytesRequested: 256 / 8
                        ));
                    User newUser = new User()
                    {
                        FirstName = _users.FirstName,
                        LastName = _users.LastName,
                        Phone = _users.Phone,
                        Email = _users.Email,
                        Address = _users.Address,
                        Avatar = "",
                        Password = hashed,
                        Role = 2,

                    };

                    _db.Users.Add(newUser);
                    _db.SaveChanges();
                    return Redirect("/Authentication/Login");
                }

                else
                {
                    return new ContentResult()
                    {
                        StatusCode = 19,
                        Content = "Email exists",
                        ContentType = "text/plain",
                    };
                }

            }
            return View();
        }

        //Get : login
        
        public IActionResult Login()
        {
            return View();
        }

    }
}
