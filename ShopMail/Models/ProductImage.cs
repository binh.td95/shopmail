using System;
namespace ShopMail.Models
{
    public class ProductImage
    {
        public int ID { get; set; }
        public decimal ProductId { get; set; }
        public string? Image { get; set; }
        public int Sort { get; set; }
    }
}