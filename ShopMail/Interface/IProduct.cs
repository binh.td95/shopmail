﻿using System;
using Microsoft.AspNetCore.Mvc;
using ShopMail.Models;

namespace ShopMail.Interface
{
    public interface IProduct
    {
        public Task<List<Product>> GetAllProductAsync();

        public Task<Product> GetProductAsync(int id);

        public Task<Product> AddProductAsync(Product model);

        public Task UpdateProductAsync(int id, Product model);

        public Task DeleteProductAsync(int id);
    }
}


