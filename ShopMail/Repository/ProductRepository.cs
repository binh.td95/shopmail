﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ShopMail.Data;
using ShopMail.Interface;
using ShopMail.Models;
using System.Threading.Tasks;

namespace ShopMail.Repository
{
    public class ProductRepository : IProduct
    {
        private readonly ShopDbContext _context;
        public ProductRepository(ShopDbContext context)
        {
            _context = context;
            
        }

        [HttpGet]
        public async Task<List<Product>> GetAllProductAsync()
        {
            return await _context.Products.ToListAsync();
        }


        [HttpGet("{id}", Name = "GetProductById")]
        public async Task<Product> GetProductAsync(int id)
        {
            return await _context.Products.FindAsync(id);
        }

        [HttpPost]
        public async Task<Product> AddProductAsync(Product product)
        {

            _context.Products.Add(product);
            await _context.SaveChangesAsync();
            return product;
        }

        [HttpPut]
        public async Task UpdateProductAsync(int id, Product model)
        {
            var existingProduct = await _context.Products.FindAsync(id);
            if (existingProduct != null)
            {
               
                existingProduct.Name = model.Name;
                existingProduct.Price = model.Price;

                await _context.SaveChangesAsync();
            }
        }

        [HttpDelete]
        public async Task DeleteProductAsync(int id)
        {
            var product = await _context.Products.FindAsync(id);
            if (product != null)
            {
                _context.Products.Remove(product);
                await _context.SaveChangesAsync();
            }
        }


    }
}

