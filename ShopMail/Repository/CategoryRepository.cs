﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ShopMail.Data;
using ShopMail.Interface;
using ShopMail.Models;

namespace ShopMail.Repository
{
    public class CategoryRepository : ICategory
    {
        private readonly ShopDbContext _context;

        public CategoryRepository(ShopDbContext context)
        {
            _context = context;

        }

        [HttpGet]
        public async Task<List<Category>> GetAllCategoryAsync()
        {
            return await _context.Categories.ToListAsync();
        }


        [HttpGet("{id}", Name = "GetCategoryById")]
        public async Task<Category> GetCategoryAsync(int id)
        {
            return await _context.Categories.FindAsync(id);
        }

        [HttpPost]
        public async Task<Category> AddCategoryAsync(Category Category)
        {

            _context.Categories.Add(Category);
            await _context.SaveChangesAsync();
            return Category;
        }

        [HttpPut]
        public async Task UpdateCategoryAsync(int id, Category model)
        {
            var existingCategory = await _context.Categories.FindAsync(id);
            if (existingCategory != null)
            {

                existingCategory.Name = model.Name;
             

                await _context.SaveChangesAsync();
            }
        }

        [HttpDelete]
        public async Task DeleteCategoryAsync(int id)
        {
            var Category = await _context.Categories.FindAsync(id);
            if (Category != null)
            {
                _context.Categories.Remove(Category);
                await _context.SaveChangesAsync();
            }
        }
    }
}

